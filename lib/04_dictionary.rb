class Dictionary
  attr_accessor :entries

  def initialize
    @entries = {}
  end

  def add(new_entries)
    if new_entries.is_a?(String)
      @entries[new_entries] = nil
    elsif new_entries.is_a?(Hash)
      @entries.merge!(new_entries)
    end

  end

  def keywords
    @entries.keys.map {|word| word.to_s}.sort
  end

  def find(k)
    @entries.select  do |key, value|
       key.match(k)
     end
  end

  def include?(k)
    @entries.has_key?(k)
  end

  def printable
    result = []
    @entries.sort.each  do |k, v|
      result << %Q{[#{k}] "#{@entries[k]}"}

    end
     result.join("\n")
  end
end


if __FILE__ == $PROGRAM_NAME

  @d = Dictionary.new
  p @d.entries
  # @d.add("name", "Liba")
  @d.add("zebra" => "African land animal with stripes")
  @d.add("fish" => "aquatic animal")
  @d.add("apple" => "fruit")
  # @d.entries.inspect
  # p @d.find("name")
  @d.printable


end
