
class Temperature

  def initialize(options = {})
    @options = options
    # @other_temp = self.class.from_celsius(7)
  end

  def in_fahrenheit
    @options.key?(:f) ? @options[:f] : (@options[:c] * 9 / 5.0) + 32
  end

  def in_celsius
    @options.key?(:c) ? @options[:c] : 5 * (@options[:f] - 32) / 9.0
  end
#
  def self.from_celsius(temp)
    self.new(c: temp)
  end
#
  def self.from_fahrenheit(temp)
    self.new(f: temp)
  end
end
#
class Fahrenheit < Temperature
  def initialize(temp)
    super(:f => temp)
    # self.fahrenheit = temp
  end

end
#
class Celsius < Temperature
  def initialize(temp)
    super(:c => temp)
    # self.celsius = temp
  end
end












# class Temperature
#   attr_accessor :celsius, :fahrenheit
#
#   def initialize(options = {})
#     if options[:f]
#       @fahrenheit = options[:f]
#     else
#       @celsius = options[:c]
#     end
#   end
#
#   def self.from_celsius(temp)
#     self.new(c: temp)
#   end
#
#   def self.from_fahrenheit(temp)
#     self.new(f: temp)
#   end
#
#   def self.ftoc(temp)
#     5 * (temp - 32) / 9.0
#   end
#
#   def self.ctof(temp)
#     32 + temp * 9 / 5.0
#   end
#
#
#   def in_fahrenheit
#     self.class.ctof(@temperature)
#   end
#
#   def in_celsius
#     self.class.ftoc(@temperature)
#   end
#
# end
#
# end
#
#
# # def self.human_to_dog_ratios
# #   # get the equivalent of one dog or human year in terms of the
# #   # other
# #   dog_to_human_ratio = self.dog_years_to_human_years(1)
# #   human_to_dog_ratio = self.human_years_to_dog_years(1)
# #
# #   { :dog_ratio => dog_to_human_ratio,
# #     :human_ratio => human_to_dog_ratio }
# # end
#
# if __FILE__ == $PROGRAM_NAME
# p t = Temperature.new(f: 32)
# p t.self.in_celsius
#
#
# end
