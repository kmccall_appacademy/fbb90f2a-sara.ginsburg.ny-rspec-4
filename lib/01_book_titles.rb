# require 'byebug'
class Book
  LOWER_WORDS  = ["and", "a", "an", "the", "in", "of"]

  attr_accessor :title


  def title=(title)
    title_arr = []

    title.split(" ").map.each_with_index do |word, i|
      word.capitalize! if !LOWER_WORDS.include?(word) || i == 0 || word.upcase == "I"
      title_arr << word
    end
    p title_arr.join(" ")
    @title = title_arr.join(" ")
  end
end

if __FILE__ == $PROGRAM_NAME

  name = "what i wish i knew when i was 20"
  book = Book.new
  book.title(name)
end
