class Timer

  attr_accessor :seconds

  def initialize(seconds = 0)
    @seconds = seconds
  end


  def time_string
    time_arr = []

    while @seconds > 0
      n = @seconds % 60
      time_arr.unshift(padded(n))
      @seconds /= 60
    end
    until time_arr.length == 3
      time_arr.unshift(padded(0))
    end
    time_arr.join(":")
  end

  def padded(n)

    n.to_s.length < 2? "0" + n.to_s : n.to_s
  end
end

if __FILE__ == $PROGRAM_NAME
  @t = Timer.new
  p @t.time_string(4000)
end
